# SampleSpotify

Bugloos co. test project 

-Why did i build this project?
    
    i've build this project to give enough reasons to Bugloos co. to hire me 
    
-What problem does it solve?
    
    it has fast-created music player so it can makes you comfort to add new musics and albumes and listen to them, hope you enjoy
    
-What did you learn?

    i've learned from this sample alot. AVFoundation is powerfull built-in framework to play with. i've found ZSticky bug and because of time difficulties i didn't fixed that in this project.
    
#Documents: 

Main: 
        
    Main is the project tab handler (i prefer customize view than usin UITabbarController because it has lots of limitations)
    
    
Tabbar:

    tabbar is UIView that handle item selection and changes
    
CurrentMusic:

    it is UIView that helps you access the current music which is playing from every controller of app everywhere instead of Player Controller.

Home:
    
    home is main page of app you can customize it how ever you like but for now it is simple as possible.
    
List: 
    
    list is where you can see musics of an album and choose the right one.
    list has customized collectionViewLayout (named ZStickHeader) to handle how much the header is collapsed and gives you the progress.
    
Player:

    player is usable music player that helps you control the music.
    
K: 

    all project constants.
    
Settings:

    setting is singleton class which helps you save the data in your localStorage using UserDefauls
    
AppConstants:

    "app manager" is brief and complete definition.
    
MusicManager: 

    it is singleton class so you can access the current playing music everywhere in app, it uses AVAudioPlayer to control the musics.
    i've used rxSwift so everywhere you need the data you can only listen to Observable contents.
    
DataSource:

    mock data to test the app 
    
UIComponents:

    it contains the reusable UI codes
    
Z: 

    because of my nickname "Zee" i've put my developed and reusable codes here (and sometimes in Extension Folder).
    


- YOU Want to start the app?

        - pull from the repository
        - open terminal
        - go to directory (with "cd")
        - run "pod install"
        - open the project .xcworkspace file
        - click on run button
