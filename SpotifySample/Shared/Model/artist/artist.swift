//
//  artist.swift
//  SpotifySample
//
//  Created by MohammadReza Zamanieh on 8/17/21.
//

import Foundation

extension Model {
    
    struct artist: Codable {
        
        public var id: Int
        public var name: String
        
    }
    
}
