//
//  music.file.swift
//  SpotifySample
//
//  Created by MohammadReza Zamanieh on 8/17/21.
//

import Foundation


extension Model.music {
    
    struct file: Codable {
        
        public var name: String
        public var `extension`: String 
        
    }
    
}
