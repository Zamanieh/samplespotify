//
//  music.swift
//  SpotifySample
//
//  Created by MohammadReza Zamanieh on 8/17/21.
//

import Foundation


extension Model {
    
    
    struct music: Codable {
        
        public var id: Int
        public var image: String
        public var title: String
        public var description: String
        public var file: Model.music.file
        public var artist: artist
        public var featuring: [artist]
        public var isFav: Bool
        public var isForbidden: Bool
        
    }
    
}
