//
//  album.swift
//  SpotifySample
//
//  Created by MohammadReza Zamanieh on 8/17/21.
//

import Foundation


extension Model {
    
    struct album: Codable {
        
        public var id: Int
        public var title: String
        public var description: String
        public var image: String
        public var musics: [Model.music]
        public var isFav: Bool
        
    }
    
}
