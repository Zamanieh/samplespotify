//
//  BaseNavC.swift
//  Eddi-Bike
//
//  Created by MohammadReza Zamanieh on 7/21/21.
//

import UIKit


/// empty backgourd navigation bar
class BaseNavC: UINavigationController {
    
    public var effect: ZBlurVisualEffectView!
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return self.statusBarStyle
    }
    
    public var statusBarStyle: UIStatusBarStyle = .default

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .clear
        self.navigationItem.backButtonDisplayMode = .minimal
        self.navigationBar.setBackgroundImage(UIImage().withTintColor(.clear), for: .default)
        self.navigationBar.shadowImage = UIImage().withTintColor(.clear)
        self.navigationBar.isTranslucent = true
        self.navigationBar.backgroundColor = .clear
        self.navigationBar.barTintColor = .white
        
        
    }

}
