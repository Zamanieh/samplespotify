//
//  IT.swift
//  Eddi-Bike
//
//  Created by MohammadReza Zamanieh on 7/28/21.
//
import Foundation
import UIKit


/// Icon + Title
class IT: UIView {

    private var imageView: UIImageView!
    private var titleLabel: UILabel!
    private var btn: UIButton!
    
    private var icon: UIImage?
    private var title: String
    private var sizeConstraint: NSLayoutConstraint!
    
    public var color: UIColor = .app.pure_white.value {
        didSet {
            self.imageView.tintColor = color
            self.titleLabel.textColor = color
        }
    }
    
    public var font: UIFont = UIFont.boldSystemFont(ofSize: 14) {
        didSet {
            self.titleLabel.font = font
        }
    }
    
    public var imageSize: CGFloat = 12.0 {
        didSet {
            sizeConstraint.constant = imageSize
        }
    }
    
    
    init(icon: UIImage?, title: String) {
        self.icon = icon
        self.title = title
        super.init(frame: .zero)
        self.initialize()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - FUNCTIONS
    private func initialize() {
        
        imageView = UIImageView()
        imageView.image = self.icon
        imageView.tintColor = self.color
        imageView.contentMode = .scaleAspectFit
        self.addSubview(imageView)
        imageView.constrain(to: self).leading().centerY().topBottom(.greaterThanOrEqual, constant: 0, priority: .defaultLow)
        sizeConstraint = imageView.constrainSelf().widthHeight(constant: self.imageSize).constraints.first
        sizeConstraint.isActive = true
        
        titleLabel = UILabel()
        titleLabel.text = self.title
        titleLabel.textColor = self.color
        titleLabel.font = self.font
        titleLabel.textAlignment = .left
        titleLabel.numberOfLines = 0
        self.addSubview(titleLabel)
        titleLabel.constrain(to: self.imageView).xAxis(.leading, to: .trailing, constant: 8)
        titleLabel.constrain(to: self).top().trailing().bottom()
        
        btn = UIButton()
        btn.setTitle(nil, for: .normal)
        btn.setImage(nil, for: .normal)
        self.addSubview(btn)
        btn.constrain(to: self).leadingTrailingTopBottom()
        
        
    }
    
    public func addTarget(_ target: Any?, action: Selector, for controlEvent: UIControl.Event) {
        btn.addTarget(target, action: action, for: controlEvent)
    }
    
}
