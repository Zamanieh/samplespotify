//
//  AppContants.swift
//  DrYab
//
//  Created by Mr Zee on 12/12/20.
//

import Foundation
import UIKit

class AppConstant: NSObject {
    
    public static let shared = AppConstant()
    public static let k = K()
    public static let setting = Settings()
    public static let music = MusicManager()
    
    
    public func _initialize() {
        // configure back button
        UIBarButtonItem.appearance(whenContainedInInstancesOf: [UINavigationBar.self]).setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.clear], for: .normal)
        UIBarButtonItem.appearance(whenContainedInInstancesOf: [UINavigationBar.self]).tintColor = .white
        
        Z.ui.switch(window: Main.vc())
    }

    
}

