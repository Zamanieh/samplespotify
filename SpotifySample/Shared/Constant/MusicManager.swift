//
//  MusicManager.swift
//  SpotifySample
//
//  Created by MohammadReza Zamanieh on 8/17/21.
//

import Foundation
import AVFoundation
import RxCocoa

/// handle all musics events
class MusicManager: NSObject {
    
    /// to access metaData
    private var playerItem: AVPlayerItem?
    /// music player
    private var player: AVAudioPlayer?
    /// to refresh the progress
    private var timer: Timer?
    
    /// current playing music
    public var current: BehaviorRelay<Model.music?> = .init(value: nil)
    /// progress of current music playing
    public var progress: BehaviorRelay<Double?> = .init(value: 0)
    public var isPlaying: BehaviorRelay<Bool> = .init(value: false)
    public var album: Model.album?
    /// duration of music
    public var duration: TimeInterval {
        guard let p = self.player else { return .zero }
        return p.duration
    }
    
    // MARK: - FUNCTIONS
    public func play(music: Model.music? = nil, with album: Model.album? = nil) {
        if let m = AppConstant.music.current.value, let p = player, (music == nil || music?.id == m.id) {
            p.play()
            self.beginTimer()
            return
        }
        guard let m = music else { return }
        guard let url = Bundle.main.url(forResource: m.file.name, withExtension: m.file.extension) else { return }
        self.album = album
        self.playerItem = AVPlayerItem.init(url: url)
        self.current.accept(music)
        do {
            try AVAudioSession.sharedInstance().setCategory(.playback, mode: .default)
            try AVAudioSession.sharedInstance().setActive(true)
            
            player = try AVAudioPlayer(contentsOf: url)
            guard let player = player else { return }
            player.prepareToPlay()
            
            player.play()
            self.isPlaying.accept(true)
            self.beginTimer()
            
        } catch (let error) {
            print(error.localizedDescription)
            self.remove()
        }
    }
    
    public func pause() {
        timer?.invalidate()
        timer = nil
        player?.pause()
        isPlaying.accept(false)
    }
    
    public func remove() {
        self.isPlaying.accept(false)
        self.progress.accept(0)
        self.current.accept(nil)
        self.player?.stop()
        self.player = nil
        self.timer?.invalidate()
        self.timer = nil
        self.album = nil
        self.playerItem = nil
    }
    
    public func next(music: Model.music) {
        self.play(music: music, with: self.album)
    }
    
    public func previous(music: Model.music? = nil) {
        guard let p = self.progress.value else { return }
        guard p < 0.1 else { self.seek(at: .zero); return }
        if let m = music {
            self.play(music: m, with: self.album)
        } else {
            self.pause()
        }
    }
    
    public func seek(at time: TimeInterval) {
        self.pause()
        self.player?.currentTime = time
        self.play()
    }
    
    public func seek(to progress: Double) {
        guard let p = self.player else { return }
        let time: TimeInterval = progress * p.duration
        self.seek(at: time)
    }
    
    public func artWork() -> UIImage? {
        guard let p = self.playerItem else { return nil }
        let meta = p.asset.metadata
        guard let item = meta.first(where: {$0.commonKey == .commonKeyArtwork }) else { return nil }
        guard let data = item.value as? Data else { return nil }
        return UIImage(data: data)
    }
    
    
    
    // MARK: - PRIVATE FUNCTIONS
    private func beginTimer() {
        self.timer = Timer.init(timeInterval: 0.5, target: self, selector: #selector(self.onTimer(_:)), userInfo: nil, repeats: true)
        self.timer?.tolerance = 0.2
        RunLoop.main.add(self.timer!, forMode: .default)
        
    }
    
    @objc private func onTimer(_ sender: Timer) {
        guard let p = player else { return }
        let currentTime = p.currentTime
        let duration = p.duration
        self.progress.accept(currentTime / duration)
        self.isPlaying.accept(p.isPlaying)
    }
    
}
