//
//  Color.swift
//  cleaning-service
//
//  Created by Mr Zee on 11/21/20.
//

import Foundation
import UIKit

extension UIColor {
    
    // MARK: - APP COLOR
    enum app: String {
            
        public var value: UIColor {
            get { return UIColor.init(hex: self.rawValue) }
        }
        
        public var dynamic: UIColor {
            get {
                return UIColor.init(dynamicProvider: { trait in
                    switch trait.userInterfaceStyle {
                    case .dark:
                        return UIColor.init(hex: self.rawValue).colorInvert()
                    default:
                        return UIColor.init(hex: self.rawValue)
                    }
                })
            }
        }
        
        case pure_black = "000000"
        case black = "222327"
        case white = "F4F4F6"
        case pure_white = "FFFFFF"
        case medium_gray = "C8C8C8"
        case gray = "6C6B6B"
        case lightGray = "E5E5EA"
        case green = "00bd46"
        case red = "ca0025"
    }
    
    
    // MARK: - EXTENSIONS
    convenience init(hex: String, alpha: CGFloat = 1.0) {
        var hexFormatted: String = hex.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).uppercased()

        if hexFormatted.hasPrefix("#") {
            hexFormatted = String(hexFormatted.dropFirst())
        }

        assert(hexFormatted.count == 6, "Invalid hex code used.")

        var rgbValue: UInt64 = 0
        Scanner(string: hexFormatted).scanHexInt64(&rgbValue)

        self.init(red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
                  green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
                  blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
                  alpha: alpha)
    }
    
    func colorInvert() -> UIColor {
        var alpha: CGFloat = 1.0

        var red: CGFloat = 0.0, green: CGFloat = 0.0, blue: CGFloat = 0.0
        if self.getRed(&red, green: &green, blue: &blue, alpha: &alpha) {
            return UIColor(red: 1.0 - red, green: 1.0 - green, blue: 1.0 - blue, alpha: alpha)
        }

        var hue: CGFloat = 0.0, saturation: CGFloat = 0.0, brightness: CGFloat = 0.0
        if self.getHue(&hue, saturation: &saturation, brightness: &brightness, alpha: &alpha) {
            return UIColor(hue: 1.0 - hue, saturation: 1.0 - saturation, brightness: 1.0 - brightness, alpha: alpha)
        }

        var white: CGFloat = 0.0
        if self.getWhite(&white, alpha: &alpha) {
            return UIColor(white: 1.0 - white, alpha: alpha)
        }

        return self
    }
    
    
}


