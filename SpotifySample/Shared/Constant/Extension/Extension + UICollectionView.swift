//
//  Extension + UICollectionView.swift
//  SpotifySample
//
//  Created by MohammadReza Zamanieh on 8/17/21.
//

import Foundation
import UIKit

extension UICollectionView {
    
    
    public func numberOfItemsInRow(interItemSpacing: CGFloat = 12, minCellWidth: CGFloat) -> Int {
        let width = self.frame.size.width - self.contentInset.left - self.contentInset.right
        let numberOfItems = (width + interItemSpacing) / (minCellWidth + interItemSpacing)
        return Int(floor(numberOfItems))
    }
    
    public func itemWidth(interItemSpacing: CGFloat = 12, minCellWidth: CGFloat)  -> CGFloat {
        let availableWidth = self.frame.size.width - self.contentInset.left - self.contentInset.right
        let numberOfItems = numberOfItemsInRow(interItemSpacing: interItemSpacing, minCellWidth: minCellWidth)
        let width = availableWidth - ((CGFloat(numberOfItems - 1)*interItemSpacing))
        return width / CGFloat(numberOfItems)
    }
    
}
