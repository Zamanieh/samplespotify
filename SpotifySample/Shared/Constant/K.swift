//
//  K.swift
//  SpotifySample
//
//  Created by MohammadReza Zamanieh on 8/17/21.
//

import Foundation
import UIKit


struct K {
    
    
    var cornerRadius: CGFloat = 8.0
    var currentMusicHeight: CGFloat = 50
    
}
