//
//  JSON.swift
//  SpotifySample
//
//  Created by MohammadReza Zamanieh on 8/18/21.
//

import Foundation
import UIKit


/// mock data
class DataSource: NSObject {
    
    public static let shared = DataSource()
    
    // MARK: - ARTIST
    let sia: Model.artist = .init(id: 0, name: "Sia")
    let adele: Model.artist = .init(id: 1, name: "Adele")
    let taylor: Model.artist = .init(id: 2, name: "Taylor Swift")
    let selena: Model.artist = .init(id: 3, name: "Selena Gomez")
    let doja: Model.artist = .init(id: 4, name: "Doja Cat")
    
    
    
    
    // MARK: - ALBUM
    var mix: Model.album {
        get {
            return .init(id: 0, title: "Daily Mix 1", description: "MADE FOR MORTEZA SAKIAN", image: UIImage.app.mix_cover.rawValue, musics: [willow, freak, hello, symphony], isFav: false)
        }
    }
    
    
    
    
    // MARK: - MUSIC
    var willow: Model.music {
        get {
            return .init(id: 0, image: UIImage.app.music.rawValue, title: "Willow", description: "", file: .init(name: "willow", extension: "mp3"), artist: taylor, featuring: [], isFav: false, isForbidden: false)
        }
    }
    var freak: Model.music {
        get {
            return .init(id: 1, image: UIImage.app.music.rawValue, title: "Freak", description: "", file: .init(name: "freak", extension: "mp3"), artist: doja, featuring: [], isFav: true, isForbidden: true)
        }
    }
    var hello: Model.music {
        get {
            return .init(id: 2, image: UIImage.app.music.rawValue, title: "Hello", description: "", file: .init(name: "hello", extension: "mp3"), artist: adele, featuring: [], isFav: false, isForbidden: true)
        }
    }
    var symphony: Model.music {
        get {
            return .init(id: 3, image: UIImage.app.music.rawValue, title: "Symphony", description: "", file: .init(name: "symphony", extension: "mp3"), artist: sia, featuring: [taylor, selena], isFav: true, isForbidden: false)
        }
    }
}
