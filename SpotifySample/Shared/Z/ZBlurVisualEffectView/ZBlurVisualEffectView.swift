//
//  UIBlurVisualEffectView.swift
//  Modish
//
//  Created by Mr Zee on 6/13/20.
//  Copyright © 2020 nizek. All rights reserved.
//

import UIKit

/**
Initializes a new visualEffectView with Blur Effect with radius.

- Parameters:
   - radius: Radius Of Blur Effect that default value is 1.0
   - style: UIBlurEffect Style that default value is Light
*/

class ZBlurVisualEffectView: UIVisualEffectView {
    
    public var animator: UIViewPropertyAnimator!

    private var fraction: CGFloat
    private var style: UIBlurEffect.Style
    
    init(radius: CGFloat = 1.0, style: UIBlurEffect.Style = .light) {
        self.fraction = radius
        self.style = style
        super.init(effect: nil)
        _initialize()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    // MARK: - FUNCTIONS
    private func _initialize() {
        self.contentView.backgroundColor = .clear
        
        animator = .init(duration: 1, curve: .linear)
        animator.addAnimations({
            self.effect = UIBlurEffect.init(style: self.style)
        })
        self.animator.fractionComplete = self.fraction
        self.animator.pausesOnCompletion = true
        
    }

}
