//
//  ZNetwork.swift
//  Eddi-Bike
//
//  Created by MohammadReza Zamanieh on 8/11/21.
//

import Foundation
import Alamofire

class ZNetwork: NSObject {
    
    typealias completionHandler = (Data) -> Void
    typealias failureHandler<T: Encodable> = (ZNetwork.request<T>, Error, HTTPURLResponse?, Data?) -> Void
    
    private var timer: Timer!
    
    
    @discardableResult open func request<T: Encodable>(path: URL, method: HTTPMethod, body: T? = nil, headers: HTTPHeaders? = nil, option: ZNetwork.options = .init(), completion: @escaping completionHandler, failure: @escaping failureHandler<T>) -> ZNetwork.request<T>? {
        let backup = ZNetwork.request<T>.init(path: path.path, method: method, body: body, headers: headers, option: self.configure(for: option), completion: completion, failure: failure)
        print("ZNetwork is requesting to:", path, option, "\n")
        AF.request(path, method: method, parameters: self.configure(with: body), encoding: JSONEncoding.default, headers: headers){ $0.timeoutInterval = option.timeoutInterval }.validate(statusCode: 200..<300).response(queue: .main, completionHandler: {[weak self] response in
            guard let this = self else { return }
            switch response.result {
            case .success(let data):
                guard let d = data else { return }
                completion(d)
            case .failure(let err):
                guard let httpResp = response.response else { failure(backup, err, response.response, response.data); return }
                if option.shouldRetry && option.currentRetry < option.retryCount {
                    let isReachable = NetworkReachabilityManager.default?.isReachable ?? false
                    if !option.retryOnNoConnection && !isReachable { failure(backup, err, httpResp, response.data); return }
                    if option.otherStatusCodesToSkipRetry.contains(httpResp.statusCode){ failure(backup, err, httpResp, response.data); return }
                    OperationQueue.current?.underlyingQueue?.asyncAfter(deadline: .now() + option.retryDelayInterval, execute: {
                        debugPrint("ZNetwork: We are trying one more time", option)
                        this.request(path: path, method: method, body: body, headers: headers, option: this.configure(for: option), completion: completion, failure: failure)
                    })
                } else {
                    failure(backup, err, httpResp, response.data)
                }
            }
        })
        return backup
    }
    
    open func cancel() {
        AF.cancelAllRequests(completingOnQueue: .main, completion: nil)
    }
    
    private func configure(for option: ZNetwork.options) -> ZNetwork.options {
        var new_opt = option
        new_opt.currentRetry += 1
        new_opt.retryDelayInterval = Double(Z.math.fib(new_opt.currentRetry))
        return new_opt
    }
    
    private func configure<T: Encodable>(with body: T?) -> Dictionary<String, Any>? {
        if let b = try? body?.asDictionary() {
            return b
        }
        return nil
    }
    
    
    
}
