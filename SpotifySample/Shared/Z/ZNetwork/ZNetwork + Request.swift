//
//  ZNetwork + Request.swift
//  Eddi-Bike
//
//  Created by MohammadReza Zamanieh on 8/15/21.
//

import Foundation
import Alamofire


extension ZNetwork {
    
    struct request<T: Encodable> {
        
        var path: String
        var method: HTTPMethod
        var body: T? = nil
        var headers: HTTPHeaders? = nil
        var option: options = .init()
        var completion: completionHandler
        var failure: failureHandler<T>
        
    }
    
}
