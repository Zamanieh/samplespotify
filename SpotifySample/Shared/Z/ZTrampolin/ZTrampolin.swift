//
//  Trampolin.swift
//  Servus
//
//  Created by Mr Zee on 11/23/20.
//

import Foundation


class ZTrampolin<T: NSObjectProtocol>: NSObject {
    
    public weak var delegate: T?
    
    init(delegate: T?) {
        self.delegate = delegate
        super.init()
    }
    
    override func responds(to aSelector: Selector!) -> Bool {
        if !class_respondsToSelector(self.classForCoder, aSelector) {
            return self.delegate?.responds(to: aSelector) ?? false
        } else {
            return true
        }
    }
    
    override func forwardingTarget(for aSelector: Selector!) -> Any? {
        if class_respondsToSelector(self.classForCoder, aSelector) {
            return self
        }
        return (self.delegate != nil) ? self.delegate : super.forwardingTarget(for: aSelector)
    }
    
}
