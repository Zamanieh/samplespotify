//
//  PageControlCell.swift
//  Servus
//
//  Created by Mr Zee on 11/22/20.
//

import UIKit

class ZPageControlCell: UICollectionViewCell {
    
    private var indicator: UIView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.initialize()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        indicator.layer.cornerRadius = self.frame.size.height / 2
    }
    
    override func apply(_ layoutAttributes: UICollectionViewLayoutAttributes) {
        super.apply(layoutAttributes)
        guard let attr = layoutAttributes as? ZStickyHeader.CollectionView.LayoutAttributes else { return }
        UIView.animate(withDuration: 0.3, animations: {
            self.selected(with: attr.progress)
        })
    }
    
    // MARK: - FUNCTIONS
    private func initialize() {
        self.backgroundColor = .clear
        self.contentView.backgroundColor = .clear
        
        indicator = UIView()
        indicator.backgroundColor = UIColor.app.black.value
        self.contentView.addSubview(indicator)
        indicator.translatesAutoresizingMaskIntoConstraints = false
        indicator.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor).isActive = true
        indicator.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor).isActive = true
        indicator.topAnchor.constraint(equalTo: self.contentView.topAnchor).isActive = true
        indicator.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor).isActive = true
    }
    
    private func selected(with progress: CGFloat) {
        let value = Z.math.lMapValue(value: progress, srcLow: 0, srcHigh: 1, dstLow: 0.13, dstHigh: 1)
        self.indicator.backgroundColor = UIColor.app.black.value.withAlphaComponent(value)
    }
    
}
