//
//  ClassHeaderImageView.swift
//  kids
//
//  Created by Mr.Zee on 2/2/20.
//  Copyright © 2020 com.nizek. All rights reserved.
//

import UIKit
import SDWebImage

class ZBouncyImageView: UIView {

    
    private var mImage: UIImageView!
    
    private var image: ZBouncyImageSlider.Image
    private var imageContentMode: UIView.ContentMode
    
    init(image: ZBouncyImageSlider.Image, contentMode: UIView.ContentMode = .scaleAspectFill) {
        self.image = image
        self.imageContentMode = contentMode
        super.init(frame: .zero)
        self.initialize()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - FUNCTIONS
    private func initialize() {
        
        mImage = UIImageView()
        mImage.contentMode = self.imageContentMode
        mImage.clipsToBounds = true
        self.addSubview(mImage)
        mImage.constrain(to: self).leadingTrailingTopBottom()
        
        
        if let name = image.name {
            self.mImage.image = UIImage(named: name)
            return
        }
        if let url = image.url {
            self.mImage.sd_setImage(with: URL(string: url), completed: nil)
        }
    }

}
