//
//  List.header.swift
//  SpotifySample
//
//  Created by MohammadReza Zamanieh on 8/17/21.
//

import Foundation
import UIKit

fileprivate var imageSize: CGFloat = 160
fileprivate var halfBtnSize: CGFloat = 30

protocol ListHeaderDelegate: NSObject {
    
    func header(_ header: List.header, playWith album: Model.album)
    
}

extension List {
    
    class header: ZCollectionReusableView {
        
        private var container: UIView!
        private var imageView: UIImageView!
        private var titleLabel: UILabel!
        private var descLabel: UILabel!
        private var playBtn: UIButton!
        
        private var gradient: CAGradientLayer!
        
        private var ds: Model.album!
        private var titleTop: NSLayoutConstraint!
        private var imageConst: NSLayoutConstraint!
        private var descBottom: NSLayoutConstraint!
        
        public weak var delegate: ListHeaderDelegate?
        
        override func initialize() {
            super.initialize()
            self._initialize()
        }
        
        override func apply(_ layoutAttributes: UICollectionViewLayoutAttributes) {
            super.apply(layoutAttributes)
            guard let attr = layoutAttributes as? ZStickyHeader.CollectionView.LayoutAttributes else { return }
            self.refresh(with: attr.collapseProgress)
        }
        
        override func layoutSubviews() {
            super.layoutSubviews()
            self.gradient.frame = self.container.bounds
        }
        
        // MARK: - FUNCTIONS
        private func _initialize() {
            self.clipsToBounds = false
            
            container = UIView()
            self.addSubview(container)
            container.constrain(to: self).leadingTrailing().top().bottom(constant: halfBtnSize)
            
            gradient = CAGradientLayer()
            gradient.colors = []
            container.layer.addSublayer(gradient)
            
            imageView = UIImageView()
            imageView.contentMode = .scaleAspectFill
            imageView.clipsToBounds = true
            imageView.backgroundColor = .red
            self.container.addSubview(imageView)
            imageView.constrain(to: self.container).centerX().leadingTrailing(.greaterThanOrEqual).top(constant: UINavigationController.navBarHeight + UIDevice.current.statusBarHeight + 24)
            imageView.constrainSelf().aspectRatio(1)
            imageConst = imageView.constrainSelf().width(constant: imageSize).constraints.first
            imageConst.isActive = true
            
            titleLabel = UILabel()
            titleLabel.textColor = UIColor.app.pure_white.value
            titleLabel.font = UIFont.boldSystemFont(ofSize: 18)
            titleLabel.textAlignment = .center
            titleLabel.numberOfLines = 0
            self.container.addSubview(titleLabel)
            titleLabel.constrain(to: self.container).leadingTrailing(constant: 12)
            titleTop = titleLabel.constrain(to: self.container).top(constant: imageSize + 48 + UINavigationController.navBarHeight + UIDevice.current.statusBarHeight).constraints.first // 24 y from image + 24 image from top
            titleTop.isActive = true
            
            descLabel = UILabel()
            descLabel.textColor = UIColor.app.lightGray.value
            descLabel.font = UIFont.boldSystemFont(ofSize: 14)
            descLabel.textAlignment = .center
            descLabel.numberOfLines = 0
            self.container.addSubview(descLabel)
            descLabel.constrain(to: self.container).leadingTrailing(constant: 12)
            descBottom = descLabel.constrain(to: self.container).bottom(constant: halfBtnSize).constraints.first
            descBottom.isActive = true
            descLabel.constrain(to: self.titleLabel).yAxis(.top, to: .bottom, constant: 12)
            
            playBtn = UIButton()
            playBtn.setTitle("PLAY", for: .normal)
            playBtn.setTitleColor(.app.pure_white.value, for: .normal)
            playBtn.backgroundColor = .app.green.value
            playBtn.setImage(nil, for: .normal)
            playBtn.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
            playBtn.layer.cornerRadius = halfBtnSize
            playBtn.addTarget(self, action: #selector(playBtnTapped(_:)), for: .touchUpInside)
            self.addSubview(playBtn)
            playBtn.constrain(to: self).centerX()
            playBtn.constrain(to: self.container).yAxis(.centerY, to: .bottom)
            playBtn.constrainSelf().height(constant: halfBtnSize * 2).width(constant: 150)
            
        }
        
        /// position view with collapse progress
        private func refresh(with progress: CGFloat) {
            let imageProg = Z.math.lMapValue(value: 1-progress, srcLow: 0, srcHigh: 1, dstLow: 0, dstHigh: imageSize)
            let titleTopMin = UIDevice.current.statusBarHeight + ((UINavigationController.navBarHeight / 2) - (self.titleLabel.frame.size.height / 2))
            let titleProg = Z.math.lMapValue(value: 1-progress, srcLow: 0, srcHigh: 1, dstLow: titleTopMin, dstHigh: imageSize + 48 + UINavigationController.navBarHeight + UIDevice.current.statusBarHeight)
            let descBottomMin = titleTopMin + titleLabel.frame.size.height - UIDevice.current.statusBarHeight - self.descLabel.frame.size.height
            let descProg = Z.math.lMapValue(value: 1-progress, srcLow: 0, srcHigh: 1, dstLow: -descBottomMin, dstHigh: halfBtnSize)
            imageConst.constant = imageProg
            titleTop.constant = titleProg
            descBottom.constant = descProg
            self.imageView.alpha = 1-progress
            self.descLabel.alpha = 1-progress
        }
        
        @objc private func playBtnTapped(_ sender: UIButton) {
            delegate?.header(self, playWith: self.ds)
        }
        
        public func configure(with album: Model.album) {
            self.ds = album
            self.imageView.image = UIImage(named: album.image)
            self.titleLabel.text = album.title
            self.descLabel.text = album.description
            if let c = self.imageView.image?.averageColor {
                self.gradient.colors = [c.withAlphaComponent(0.8).cgColor, c.withAlphaComponent(0.6).cgColor, c.withAlphaComponent(0.4).cgColor, c.withAlphaComponent(0.2).cgColor, c.withAlphaComponent(0.02).cgColor]
            }
        }
        
    }
    
}
