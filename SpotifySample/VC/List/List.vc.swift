//
//  List.vc.swift
//  SpotifySample
//
//  Created by MohammadReza Zamanieh on 8/17/21.
//

import Foundation
import UIKit


extension List {
    
    class vc: ZBaseVC, ZStickyHeaderCollectionViewFlowLayoutDelegate, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, ListHeaderDelegate {
        
        private var more: UIBarButtonItem!
        private var fav: UIBarButtonItem!
        private var layout: ZStickyHeader.CollectionView.FlowLayout!
        private var cv: UICollectionView!
        
        private var album: Model.album
        
        init(album: Model.album) {
            self.album = album
            super.init(nibName: nil, bundle: nil)
        }
        
        required init?(coder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        override func viewDidLoad() {
            super.viewDidLoad()
            self.initialize()
        }
        
        // MARK: - FUNCTIONS
        private func initialize() {
            self.view.backgroundColor = UIColor.app.pure_black.value
            
            more = UIBarButtonItem.init(image: .app.more.value, style: .plain, target: self, action: #selector(moreBtnTapped(_:)))
            fav = UIBarButtonItem.init(image: self.album.isFav ? .app.favorite.value : .app.unFavorite.value, style: .plain, target: self, action: #selector(favBtnTapped(_:)))
            fav.tintColor = self.album.isFav ? .app.red.value : .app.pure_white.value
            self.navigationItem.rightBarButtonItems = [more, fav]
            
            layout = ZStickyHeader.CollectionView.FlowLayout()
            layout.delegate = self
            
            cv = UICollectionView.init(frame: .zero, collectionViewLayout: layout)
            cv.delegate = self
            cv.dataSource = self
            cv.register(List.cell.self, forCellWithReuseIdentifier: List.cell.reuseIdentifier())
            cv.register(List.header.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: List.header.reuseIdentifier())
            cv.backgroundColor = .clear
            cv.showsVerticalScrollIndicator = false
            self.view.addSubview(cv)
            cv.constrain(to: self.view).leadingTrailing().bottom().top(constant: -UIDevice.current.statusBarHeight - UINavigationController.navBarHeight)
            
            
        }
        
        @objc private func moreBtnTapped(_ sender: UIBarButtonItem) {
            
        }
        
        @objc private func favBtnTapped(_ sender: UIBarButtonItem) {
            self.album.isFav.toggle()
            self.fav.image = self.album.isFav ? .app.favorite.value : .app.unFavorite.value
            self.fav.tintColor = self.album.isFav ? .app.red.value : .app.pure_white.value
        }
        
        // MARK: - CV DELEGATE, DATASOURCE
        func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            return self.album.musics.count
        }
        
        func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: List.cell.reuseIdentifier(), for: indexPath) as! List.cell
            cell.configure(with: self.album.musics[indexPath.item])
            return cell
        }
        
        func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
            let vc = Player.vc.init(music: self.album.musics[indexPath.item], album: self.album)
            self.present(vc, animated: true, completion: nil)
        }
        
        func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
            if kind == UICollectionView.elementKindSectionHeader {
                let v = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: List.header.reuseIdentifier(), for: indexPath) as! List.header
                v.configure(with: self.album)
                v.delegate = self
                return v
            } else {
                fatalError("not configured element kind")
            }
        }
        
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
            return .init(width: self.view.frame.size.width, height: 68)
        }
        
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
            return 12
        }
        
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
            return 12
        }
        
        // MARK: - STICKY HEADER DELEGATE
        func headerSize(layout: ZStickyHeader.CollectionView.FlowLayout, collectionViewSize: CGSize) -> ZStickyHeader.CollectionView.HeaderSize {
            let defaultHeight: CGFloat = UINavigationController.navBarHeight + UIDevice.current.statusBarHeight
            return .init(minimumHeight: defaultHeight + 65, normalHeight: defaultHeight + 330)
        }
        
        // MARK: - HEADER DELEGATE
        func header(_ header: List.header, playWith album: Model.album) {
            let random = Int.random(in: 0..<album.musics.count)
            let music = album.musics[random]
            let vc = Player.vc.init(music: music, album: album)
            self.present(vc, animated: true, completion: nil)
        }
        
    }
    
    
}
