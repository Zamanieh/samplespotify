//
//  List.cell.swift
//  SpotifySample
//
//  Created by MohammadReza Zamanieh on 8/17/21.
//

import Foundation
import UIKit


extension List {
    
    class cell: ZCollectionViewCell {
        
        private var titleLabel: UILabel!
        private var descLabel: UILabel!
        private var moreBtn: UIButton!
        private var forbidBtn: UIButton!
        private var favBtn: UIButton!
        
        private var ds: Model.music!
        
        override func initialize() {
            super.initialize()
            self._initialize()
        }
        
        // MARK: - FUNCTIONS
        private func _initialize() {
            
            titleLabel = UILabel()
            titleLabel.textColor = UIColor.app.pure_white.value
            titleLabel.font = UIFont.systemFont(ofSize: 14)
            titleLabel.textAlignment = .left
            self.contentView.addSubview(titleLabel)
            titleLabel.constrain(to: self.contentView).top(constant: 12).leading(constant: 12)
            
            descLabel = UILabel()
            descLabel.textColor = UIColor.app.lightGray.value
            descLabel.font = UIFont.systemFont(ofSize: 12)
            descLabel.textAlignment = .left
            self.contentView.addSubview(descLabel)
            descLabel.constrain(to: self.contentView).leading(constant: 12).bottom(constant: 12)
            descLabel.constrain(to: self.titleLabel).yAxis(.top, to: .bottom, relation: .greaterThanOrEqual, constant: 4)
            
            moreBtn = UIButton()
            moreBtn.setTitle(nil, for: .normal)
            moreBtn.setImage(.app.more.value, for: .normal)
            moreBtn.tintColor = .app.lightGray.value
            self.contentView.addSubview(moreBtn)
            moreBtn.constrain(to: self.contentView).trailing(constant: 12).topBottom(constant: 16)
            
            forbidBtn = UIButton()
            forbidBtn.setTitle(nil, for: .normal)
            forbidBtn.setImage(.app.forbidden.value, for: .normal)
            forbidBtn.tintColor = .app.lightGray.value
            forbidBtn.addTarget(self, action: #selector(forbidBtnTapped(_:)), for: .touchUpInside)
            self.contentView.addSubview(forbidBtn)
            forbidBtn.constrain(to: self.moreBtn).topBottom().xAxis(.trailing, to: .leading, constant: -8)
            
            favBtn = UIButton()
            favBtn.setTitle(nil, for: .normal)
            favBtn.setImage(.app.unFavorite.value, for: .normal)
            favBtn.tintColor = .app.lightGray.value
            favBtn.addTarget(self, action: #selector(favBtnTapped(_:)), for: .touchUpInside)
            self.contentView.addSubview(favBtn)
            favBtn.constrain(to: self.forbidBtn).topBottom().xAxis(.trailing, to: .leading, constant: -8)
            
        }
        
        public func configure(with model: Model.music) {
            self.ds = model
            self.titleLabel.text = model.title
            self.descLabel.text = model.artist.name
            self.forbidBtn.tintColor = model.isForbidden ? UIColor.app.red.value : UIColor.app.lightGray.value
            self.favBtn.setImage(model.isFav ? .app.favorite.value : .app.unFavorite.value, for: .normal)
            self.favBtn.tintColor = model.isFav ? .app.red.value : .app.lightGray.value
        }
        
        @objc private func favBtnTapped(_ sender: UIBarButtonItem) {
            self.ds.isFav.toggle()
            self.favBtn.setImage(self.ds.isFav ? .app.favorite.value : .app.unFavorite.value, for: .normal)
            self.favBtn.tintColor = self.ds.isFav ? .app.red.value : .app.lightGray.value
        }
        
        @objc private func forbidBtnTapped(_ sender: UIBarButtonItem) {
            self.ds.isForbidden.toggle()
            self.forbidBtn.tintColor = self.ds.isFav ? .app.red.value : .app.lightGray.value
        }
        
    }
    
}
