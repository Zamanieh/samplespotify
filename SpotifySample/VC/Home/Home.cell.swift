//
//  Home.cell.swift
//  SpotifySample
//
//  Created by MohammadReza Zamanieh on 8/17/21.
//

import Foundation
import UIKit


extension Home {
    
    class cell: ZCollectionViewCell {
        
        private var shadow: UIView!
        private var imageView: UIImageView!
        
        
        override func initialize() {
            super.initialize()
            self._initialize()
        }
        
        // MARK: - FUNCTIONS
        private func _initialize() {
            self.clipsToBounds = false
            self.contentView.clipsToBounds = false
            
            shadow = UIView()
            shadow.backgroundColor = .app.black.value
            shadow.layer.cornerRadius = AppConstant.k.cornerRadius
            shadow.dropShadow(.app.white.value, opacity: 0.3, .zero, radius: 6)
            self.contentView.addSubview(shadow)
            shadow.constrain(to: self.contentView).leadingTrailingTopBottom()
            
            imageView = UIImageView()
            imageView.contentMode = .scaleAspectFill
            imageView.clipsToBounds = true
            imageView.layer.cornerRadius = AppConstant.k.cornerRadius
            self.shadow.addSubview(imageView)
            imageView.constrain(to: self.shadow).leadingTrailingTopBottom()
            
        }
        
        public func configure(with album: Model.album) {
            imageView.image = UIImage(named: album.image)
        }
    }
    
}
