//
//  Home.vc.swift
//  SpotifySample
//
//  Created by MohammadReza Zamanieh on 8/17/21.
//

import Foundation
import UIKit
import RxSwift

fileprivate var minimumCellSize: CGFloat = 140.0
fileprivate var itemSpacing: CGFloat = 24.0

extension Home {
    
    class vc: ZBaseVC, UICollectionViewDelegate, UICollectionViewDataSource {
        
        private var layout: UICollectionViewFlowLayout!
        private var cv: UICollectionView!
        
        private var ds: [Model.album] = []
        private var disposeBag = DisposeBag()
        
        override func viewDidLoad() {
            super.viewDidLoad()
            self.initialize()
        }
        
        override func viewDidLayoutSubviews() {
            super.viewDidLayoutSubviews()
            let width = cv.itemWidth(interItemSpacing: itemSpacing, minCellWidth: minimumCellSize)
            layout.itemSize = .init(width: width, height: width)
            self.cv.collectionViewLayout = layout
        }
        
        // MARK: - FUNCTIONS
        private func initialize() {
            self.view.backgroundColor = UIColor.app.pure_black.value
            self.title = "Home"
            
            layout = UICollectionViewFlowLayout()
            layout.scrollDirection = .vertical
            layout.minimumLineSpacing = itemSpacing
            layout.minimumInteritemSpacing = itemSpacing
            
            cv = UICollectionView.init(frame: .zero, collectionViewLayout: layout)
            cv.backgroundColor = .clear
            cv.delegate = self
            cv.dataSource = self
            cv.register(Home.cell.self, forCellWithReuseIdentifier: Home.cell.reuseIdentifier())
            cv.contentInset = .init(top: 12, left: 12, bottom: 0, right: 12)
            self.view.addSubview(cv)
            cv.constrain(to: self.view).leadingTrailingTopBottom()
            
            
            AppConstant.music.isPlaying.subscribe(onNext: {[weak self] bool in
                guard let this = self else { return }
                var bottom = UIDevice.current.bottomNotch + UITabBar.tabBarHeight
                if bool {
                    bottom += AppConstant.k.currentMusicHeight
                }
                this.cv.contentInset.bottom = bottom
            }).disposed(by: disposeBag)
            
            self.ds = [DataSource.shared.mix, DataSource.shared.mix]
            self.cv.reloadData()
        }
        
        // MARK: - CV DELEGATE, DATASOURCE
        func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            return self.ds.count
        }
        
        func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Home.cell.reuseIdentifier(), for: indexPath) as! Home.cell
            cell.configure(with: self.ds[indexPath.item])
            return cell
        }
        
        func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
            let vc = List.vc.init(album: self.ds[indexPath.item])
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        
    }
    
    
}
