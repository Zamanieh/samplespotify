//
//  more.swift
//  SpotifySample
//
//  Created by MohammadReza Zamanieh on 8/18/21.
//

import Foundation
import UIKit

extension Player {
    
    class more: ZBaseVC {
        
        private var gradient: CAGradientLayer!
        private var sv: UIScrollView!
        private var contentView: UIView!
        private var imageBg: UIView!
        private var imageView: UIImageView!
        private var nameLabel: UILabel!
        private var artistLabel: UILabel!
        private var st: UIStackView!
        private var like: IT!
        private var forbid: IT!
        private var sep: UIView!
        private var add: IT!
        private var viewArtist: IT!
        private var share: IT!
        private var report: IT!
        
        
        private var music: Model.music
        
        init(music: Model.music) {
            self.music = music
            super.init(nibName: nil, bundle: nil)
        }
        
        required init?(coder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        override func viewDidLoad() {
            super.viewDidLoad()
            self.initialize()
        }
        
        override func viewDidLayoutSubviews() {
            super.viewDidLayoutSubviews()
            self.gradient.frame = self.view.frame
        }
        
        // MARK: - FUNCTIONS
        private func initialize() {
            self.view.backgroundColor = .clear
            
            let tap = UITapGestureRecognizer(target: self, action: #selector(dismiss(_:)))
            tap.cancelsTouchesInView = false
            self.view.addGestureRecognizer(tap)
            
            let c = UIColor.app.pure_black.value
            gradient = CAGradientLayer()
            gradient.colors = [UIColor.clear.cgColor, c.withAlphaComponent(0.8).cgColor, c.withAlphaComponent(0.9).cgColor, c.cgColor, c.cgColor, c.cgColor]
            gradient.zPosition = -1
            self.view.layer.addSublayer(gradient)
            
            sv = UIScrollView()
            sv.backgroundColor = .clear
            self.view.addSubview(sv)
            sv.constrain(to: self.view).leadingTrailingTopBottom()
            
            contentView = UIView()
            contentView.backgroundColor = .clear
            self.sv.addSubview(contentView)
            contentView.constrain(to: self.sv).leadingTrailingTopBottom().width()
            
            imageBg = UIView()
            imageBg.backgroundColor = UIColor.app.black.value
            imageBg.dropShadow(UIColor.app.pure_black.value, opacity: 0.8, .init(width: 0, height: 4), radius: 6)
            self.contentView.addSubview(imageBg)
            imageBg.constrain(to: self.contentView).leadingTrailing(constant: 96).top(constant: 120)
            imageBg.constrainSelf().aspectRatio(1)
            
            imageView = UIImageView()
            imageView.image = .app.music.value
            imageView.contentMode = .scaleAspectFit
            imageView.clipsToBounds = true
            imageView.tintColor = UIColor.app.lightGray.value
            self.imageBg.addSubview(imageView)
            imageView.constrain(to: self.imageBg).leadingTrailingTopBottom(constant: 24)
            
            nameLabel = UILabel()
            nameLabel.text = self.music.title
            nameLabel.textColor = .app.pure_white.value
            nameLabel.font = .boldSystemFont(ofSize: 16)
            nameLabel.numberOfLines = 0
            nameLabel.textAlignment = .center
            self.contentView.addSubview(nameLabel)
            nameLabel.constrain(to: self.contentView).leadingTrailing(constant: 14)
            nameLabel.constrain(to: self.imageBg).yAxis(.top, to: .bottom, constant: 48)
            
            artistLabel = UILabel()
            artistLabel.text = self.music.artist.name
            artistLabel.textColor = .app.lightGray.value
            artistLabel.font = .boldSystemFont(ofSize: 12)
            artistLabel.numberOfLines = 0
            artistLabel.textAlignment = .center
            self.contentView.addSubview(nameLabel)
            artistLabel.constrain(to: self.contentView).leadingTrailing(constant: 16)
            artistLabel.constrain(to: self.nameLabel).yAxis(.top, to: .bottom, constant: 4)
            
            st = UIStackView()
            st.alignment = .fill
            st.axis = .vertical
            st.distribution = .fillProportionally
            st.spacing = 16
            self.contentView.addSubview(st)
            st.constrain(to: self.contentView).leadingTrailing(constant: 12).bottom(constant: 24)
            st.constrain(to: self.artistLabel).yAxis(.top, to: .bottom, constant: 24)
            
            like = IT.init(icon: .app.unFavorite.value, title: "Like")
            self.st.addArrangedSubview(like)
            
            forbid = IT.init(icon: .app.forbidden.value, title: "Hide this song")
            self.st.addArrangedSubview(forbid)
            
            sep = UIView()
            sep.backgroundColor = .app.gray.value.withAlphaComponent(0.8)
            self.st.addArrangedSubview(sep)
            sep.constrainSelf().height(constant: 1)
            
            add = IT.init(icon: .app.favorite.value, title: "Add to Playlist")
            self.st.addArrangedSubview(add)
            
            viewArtist = IT.init(icon: .app.artist.value, title: "View Artist")
            self.st.addArrangedSubview(viewArtist)
            
            share = IT.init(icon: .app.share.value, title: "Share")
            self.st.addArrangedSubview(share)
            
            report = IT.init(icon: .app.report.value, title: "Report Explicit Content")
            self.st.addArrangedSubview(report)
            
        }
        
        @objc private func dismiss(_ sender: UITapGestureRecognizer) {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
}
