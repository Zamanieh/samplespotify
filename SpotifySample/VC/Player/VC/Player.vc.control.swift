//
//  Player.vc.controller.swift
//  SpotifySample
//
//  Created by MohammadReza Zamanieh on 8/18/21.
//

import Foundation
import UIKit
import RxSwift

extension Player.vc {
    
    /// controls the music
    class control: UIView {
        
        private var favBtn: UIButton!
        private var prevBtn: UIButton!
        private var playBtn: UIButton!
        private var nextBtn: UIButton!
        private var forbidBtn: UIButton!
        
        
        private var music: Model.music
        private var album: Model.album
        private var disposeBag = DisposeBag()
        
        init(music: Model.music, album: Model.album) {
            self.music = music
            self.album = album
            super.init(frame: .zero)
            self.initialize()
        }
        
        required init?(coder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        override func layoutSubviews() {
            super.layoutSubviews()
            self.playBtn.layer.cornerRadius = self.playBtn.frame.size.height / 2
        }
        
        // MARK: - FUNCTIONS
        private func initialize() {
            
            favBtn = UIButton()
            favBtn.setTitle(nil, for: .normal)
            favBtn.tintColor = .app.pure_white.value
            favBtn.addTarget(self, action: #selector(favBtnTapped(_:)), for: .touchUpInside)
            self.addSubview(favBtn)
            favBtn.constrain(to: self).topBottom(constant: 12).leading()
            
            prevBtn = UIButton()
            prevBtn.setTitle(nil, for: .normal)
            prevBtn.setImage(.app.previous.value, for: .normal)
            prevBtn.tintColor = .app.pure_white.value
            prevBtn.addTarget(self, action: #selector(prevBtnTapped(_:)), for: .touchUpInside)
            self.addSubview(prevBtn)
            prevBtn.constrain(to: self).topBottom(constant: 8)
            prevBtn.constrain(to: self.favBtn).xAxis(.leading, to: .trailing, relation: .greaterThanOrEqual, constant: 24)
            
            playBtn = UIButton()
            playBtn.setTitle(nil, for: .normal)
            playBtn.setImage(.app.pause.value, for: .normal)
            playBtn.tintColor = .app.pure_black.value
            playBtn.backgroundColor = .app.white.value
            playBtn.imageEdgeInsets = .init(top: 12, left: 12, bottom: 12, right: 12)
            playBtn.addTarget(self, action: #selector(playBtnTapped(_:)), for: .touchUpInside)
            self.addSubview(playBtn)
            playBtn.constrain(to: self).topBottom().centerX()
            playBtn.constrainSelf().aspectRatio(1)
            playBtn.constrain(to: self.prevBtn).xAxis(.leading, to: .trailing, constant: 16)
            
            nextBtn = UIButton()
            nextBtn.setTitle(nil, for: .normal)
            nextBtn.setImage(.app.next.value, for: .normal)
            nextBtn.tintColor = .app.pure_white.value
            nextBtn.addTarget(self, action: #selector(nextBtnTapped(_:)), for: .touchUpInside)
            self.addSubview(nextBtn)
            nextBtn.constrain(to: self).topBottom(constant: 8)
            nextBtn.constrain(to: self.playBtn).xAxis(.leading, to: .trailing, constant: 16)
            
            forbidBtn = UIButton()
            forbidBtn.setTitle(nil, for: .normal)
            forbidBtn.setImage(.app.forbidden.value, for: .normal)
            forbidBtn.tintColor = self.music.isForbidden ? .red : .app.pure_white.value
            forbidBtn.addTarget(self, action: #selector(forbidBtnTapped(_:)), for: .touchUpInside)
            self.addSubview(forbidBtn)
            forbidBtn.constrain(to: self).topBottom(constant: 12).trailing()
            forbidBtn.constrain(to: self.nextBtn).xAxis(.leading, to: .trailing, relation: .greaterThanOrEqual, constant: 24)
        
            AppConstant.music.isPlaying.subscribe(onNext: {[weak self] bool in
                guard let this = self else { return }
                this.playBtn.setImage(bool ? .app.pause.value : .app.play.value, for: .normal)
            }).disposed(by: disposeBag)
        }
        
        public func configure(with model: Model.music) {
            self.music = model
            forbidBtn.tintColor = self.music.isForbidden ? .red : .app.pure_white.value
            favBtn.setImage(self.music.isFav ? .app.favorite.value : .app.unFavorite.value, for: .normal)
            favBtn.tintColor = self.music.isFav ? .app.red.value : .app.pure_white.value
        }
        
        @objc private func forbidBtnTapped(_ sender: UIButton) {
            self.music.isForbidden.toggle()
            forbidBtn.tintColor = self.music.isForbidden ? .red : .app.pure_white.value
        }
        
        @objc private func nextBtnTapped(_ sender: UIButton) {
            guard var index = self.album.musics.firstIndex(where: { $0.id == self.music.id }) else { return }
            index += 1
            if index > self.album.musics.count - 1 { index = 0; }
            AppConstant.music.next(music: self.album.musics[index])
        }
        
        @objc private func playBtnTapped(_ sender: UIButton) {
            let isPlaying = AppConstant.music.isPlaying.value
            isPlaying ? AppConstant.music.pause() : AppConstant.music.play()
        }
        
        @objc private func prevBtnTapped(_ sender: UIButton) {
            guard var index = self.album.musics.firstIndex(where: { $0.id == self.music.id }) else { return }
            index -= 1
            if index < 0 { index = self.album.musics.count - 1; }
            AppConstant.music.previous(music: self.album.musics[index])
        }
        
        @objc private func favBtnTapped(_ sender: UIButton) {
            self.music.isFav.toggle()
            favBtn.setImage(self.music.isFav ? .app.favorite.value : .app.unFavorite.value, for: .normal)
            favBtn.tintColor = self.music.isFav ? .app.red.value : .app.pure_white.value
        }
        
        
        
        
    }
    
    
}
