//
//  Player.vc.swift
//  SpotifySample
//
//  Created by MohammadReza Zamanieh on 8/18/21.
//

import Foundation
import UIKit
import RxSwift



extension Player {
    
    class vc: ZBaseVC {
        
        private var sv: UIScrollView!
        private var contentView: UIView!
        private var gradient: CAGradientLayer!
        private var navBar: Player.vc.navBar!
        private var imageBg: UIView!
        private var imageView: UIImageView!
        private var nameLabel: UILabel!
        private var artistLabel: UILabel!
        private var slider: Player.vc.slider!
        private var control: Player.vc.control!
        private var shareBtn: UIButton!
        private var pc: UIButton!
        
        private var music: Model.music
        private var album: Model.album
        private var disposeBag = DisposeBag()
        
        
        init(music: Model.music, album: Model.album) {
            self.music = music
            self.album = album
            super.init(nibName: nil, bundle: nil)
        }
        
        required init?(coder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        override func viewDidLoad() {
            super.viewDidLoad()
            self.initialize()
        }
        
        override func viewDidLayoutSubviews() {
            super.viewDidLayoutSubviews()
            gradient.frame = self.view.frame
        }
        
        // MARK: - FUNCTIONS
        private func initialize() {
            
            self.view.backgroundColor = UIColor.app.pure_black.value
            
            gradient = CAGradientLayer()
            gradient.zPosition = -1
            self.view.layer.addSublayer(gradient)
            
            self.navBar = Player.vc.navBar.init(music: self.music, album: self.album)
            self.view.addSubview(self.navBar)
            self.navBar.constrain(to: self.view).leadingTrailing().top()
            
            sv = UIScrollView()
            sv.backgroundColor = .clear
            self.view.addSubview(sv)
            sv.constrain(to: self.view).leadingTrailing().bottom()
            sv.constrain(to: self.navBar).yAxis(.top, to: .bottom)
            
            contentView = UIView()
            contentView.backgroundColor = .clear
            self.sv.addSubview(contentView)
            contentView.constrain(to: self.sv).leadingTrailingTopBottom().width()
            
            imageBg = UIView()
            imageBg.backgroundColor = UIColor.app.black.value
            imageBg.dropShadow(UIColor.app.pure_black.value, opacity: 0.8, .init(width: 0, height: 4), radius: 6)
            self.contentView.addSubview(imageBg)
            imageBg.constrain(to: self.contentView).leadingTrailing(constant: 24)
            imageBg.constrain(to: self.navBar).yAxis(.top, to: .bottom, constant: 48)
            imageBg.constrainSelf().aspectRatio(1)
            
            imageView = UIImageView()
            imageView.image = UIImage(named: self.music.image)
            imageView.contentMode = .scaleAspectFill
            imageView.clipsToBounds = true
            imageView.tintColor = UIColor.app.lightGray.value
            self.imageBg.addSubview(imageView)
            imageView.constrain(to: self.imageBg).leadingTrailingTopBottom()
            
            nameLabel = UILabel()
            nameLabel.text = self.music.title
            nameLabel.textColor = .app.pure_white.value
            nameLabel.font = .boldSystemFont(ofSize: 24)
            nameLabel.numberOfLines = 0
            nameLabel.textAlignment = .center
            self.contentView.addSubview(nameLabel)
            nameLabel.constrain(to: self.contentView).leadingTrailing(constant: 24)
            nameLabel.constrain(to: self.imageBg).yAxis(.top, to: .bottom, constant: 48)
            
            artistLabel = UILabel()
            artistLabel.text = self.music.artist.name
            artistLabel.textColor = .app.lightGray.value
            artistLabel.font = .boldSystemFont(ofSize: 20)
            artistLabel.numberOfLines = 0
            artistLabel.textAlignment = .center
            self.contentView.addSubview(nameLabel)
            artistLabel.constrain(to: self.contentView).leadingTrailing(constant: 24)
            artistLabel.constrain(to: self.nameLabel).yAxis(.top, to: .bottom, constant: 4)
            
            self.slider = Player.vc.slider()
            self.contentView.addSubview(self.slider)
            self.slider.constrain(to: self.artistLabel).yAxis(.top, to: .bottom, constant: 12)
            self.slider.constrain(to: self.contentView).leadingTrailing(constant: 24)
            
            self.control = Player.vc.control.init(music: self.music, album: self.album)
            self.contentView.addSubview(self.control)
            self.control.constrain(to: self.contentView).leadingTrailing(constant: 24)
            self.control.constrain(to: self.slider).yAxis(.top, to: .bottom, constant: 8)
            
            pc = UIButton()
            pc.setTitle(nil, for: .normal)
            pc.setImage(.app.pc.value, for: .normal)
            pc.tintColor = .app.lightGray.value
            self.contentView.addSubview(pc)
            pc.constrain(to: self.contentView).leading(constant: 24).bottom()
            pc.constrain(to: self.control).yAxis(.top, to: .bottom, constant: 36)
            
            shareBtn = UIButton()
            shareBtn.setTitle(nil, for: .normal)
            shareBtn.setImage(.app.share.value, for: .normal)
            shareBtn.tintColor = .app.lightGray.value
            self.contentView.addSubview(shareBtn)
            shareBtn.constrain(to: self.contentView).trailing(constant: 24).bottom()
            shareBtn.constrain(to: self.control).yAxis(.top, to: .bottom, constant: 36)
            
            AppConstant.music.play(music: self.music, with: self.album)
            
            AppConstant.music.current.subscribe(onNext: {[weak self] music in
                guard let this = self else { return }
                guard let m = music else { this.dismiss(animated: true, completion: nil); return }
                this.configure(with: m)
            }).disposed(by: disposeBag)
        }
        
        private func configure(with model: Model.music) {
            self.music = model
            self.nameLabel.text = model.title
            self.artistLabel.text = model.artist.name
            self.control.configure(with: model)
            if let artWork = AppConstant.music.artWork() {
                self.imageView.image = artWork
            } else {
                self.imageView.image = UIImage(named: model.image)
            }
            if let c = self.imageView.image?.averageColor {
                gradient.colors = [c.cgColor, c.withAlphaComponent(0.8).cgColor, c.withAlphaComponent(0.6).cgColor, c.withAlphaComponent(0.4).cgColor, c.withAlphaComponent(0.2).cgColor, c.withAlphaComponent(0.1).cgColor]
            }
        }
        
    }
    
}
