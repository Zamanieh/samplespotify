//
//  Player.vc.navBar.swift
//  SpotifySample
//
//  Created by MohammadReza Zamanieh on 8/18/21.
//

import Foundation
import UIKit

extension Player.vc {
    
    class navBar: UIView {
        
        private var closeBtn: UIButton!
        private var centerView: UIView!
        private var titleLabel: UILabel!
        private var albumLabel: UILabel!
        private var moreBtn: UIButton!
        
        private var music: Model.music
        private var album: Model.album
        
        init(music: Model.music, album: Model.album) {
            self.music = music
            self.album = album
            super.init(frame: .zero)
            self.initialize()
        }
        
        required init?(coder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        // MARK: - FUNCTIONS
        private func initialize() {
            self.backgroundColor = .clear
            
            closeBtn = UIButton()
            closeBtn.setTitle(nil, for: .normal)
            closeBtn.setImage(.app.close.value, for: .normal)
            closeBtn.tintColor = UIColor.app.pure_white.value
            closeBtn.addTarget(self, action: #selector(closeBtnTapped(_:)), for: .touchUpInside)
            closeBtn.imageView?.contentMode = .scaleAspectFit
            self.addSubview(closeBtn)
            closeBtn.constrain(to: self).leading(constant: 16).topBottom(constant: 8)
            
            centerView = UIView()
            centerView.backgroundColor = .clear
            self.addSubview(centerView)
            centerView.constrain(to: self).centerX().topBottom(constant: 8)
            centerView.constrain(to: self.closeBtn).xAxis(.leading, to: .trailing, relation: .greaterThanOrEqual, constant: 12)
            
            titleLabel = UILabel()
            titleLabel.textColor = .app.lightGray.value.withAlphaComponent(0.7)
            titleLabel.text = "PLAYING FROM PLAYLIST"
            titleLabel.font = UIFont.boldSystemFont(ofSize: 12)
            titleLabel.textAlignment = .center
            titleLabel.numberOfLines = 0
            self.centerView.addSubview(titleLabel)
            titleLabel.constrain(to: self.centerView).leadingTrailing().top()
            
            albumLabel = UILabel()
            albumLabel.textColor = .app.pure_white.value
            albumLabel.text = self.album.title
            albumLabel.font = UIFont.boldSystemFont(ofSize: 16)
            albumLabel.textAlignment = .center
            albumLabel.numberOfLines = 0
            self.centerView.addSubview(albumLabel)
            albumLabel.constrain(to: self.centerView).leadingTrailing().bottom()
            albumLabel.constrain(to: self.titleLabel).yAxis(.top, to: .bottom, constant: 2)
            
            moreBtn = UIButton()
            moreBtn.setTitle(nil, for: .normal)
            moreBtn.setImage(.app.more.value, for: .normal)
            moreBtn.tintColor = UIColor.app.pure_white.value
            moreBtn.addTarget(self, action: #selector(moreBtnTapped(_:)), for: .touchUpInside)
            self.addSubview(closeBtn)
            moreBtn.constrain(to: self).trailing(constant: 16).topBottom(constant: 4)
            moreBtn.constrain(to: self.centerView).xAxis(.leading, to: .trailing, relation: .greaterThanOrEqual, constant: 12)
            
        }
        
        @objc private func closeBtnTapped(_ sender: UIButton) {
            self.parentVC?.dismiss(animated: true, completion: nil)
        }
        
        @objc private func moreBtnTapped(_ sender: UIButton) {
            let vc = Player.more.init(music: self.music)
            self.parentVC?.present(vc, animated: true, completion: nil)
        }
    }
    
}
