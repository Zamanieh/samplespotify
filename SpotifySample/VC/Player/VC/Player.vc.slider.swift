//
//  Player.slider.swift
//  SpotifySample
//
//  Created by MohammadReza Zamanieh on 8/18/21.
//

import Foundation
import UIKit
import RxSwift

extension Player.vc {
    
    class slider: UIView {
        
        private var slider: UISlider!
        private var current: UILabel!
        private var total: UILabel!
        
        private var disposeBag = DisposeBag()
        
        init() {
            super.init(frame: .zero)
            self.initialize()
        }
        
        required init?(coder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        // MARK: - FUNCTIONS
        private func initialize() {
            
            slider = UISlider()
            slider.minimumValue = 0.0
            slider.maximumValue = 1.0
            slider.isContinuous = true
            slider.thumbTintColor = UIColor.app.pure_white.value
            slider.minimumTrackTintColor = .app.pure_white.value
            slider.maximumTrackTintColor = .app.pure_white.value.withAlphaComponent(0.5)
            slider.addTarget(self, action: #selector(sliderChanged(_:)), for: .valueChanged)
            self.addSubview(slider)
            slider.constrain(to: self).leadingTrailing().top()
            
            current = UILabel()
            current.textColor = UIColor.app.lightGray.value
            current.font = UIFont.systemFont(ofSize: 12)
            self.addSubview(current)
            current.constrain(to: self.slider).leading().yAxis(.top, to: .bottom, constant: 4)
            
            total = UILabel()
            total.textColor = UIColor.app.lightGray.value
            total.font = UIFont.systemFont(ofSize: 12)
            self.addSubview(total)
            total.constrain(to: self.slider).trailing().yAxis(.top, to: .bottom, constant: 4)
            total.constrain(to: self).bottom()
            
            AppConstant.music.progress.subscribe(onNext: {[weak self] progress in
                guard let this = self else { return }
                guard let p = progress else { return }
                let duration = AppConstant.music.duration
                this.current.text = this.format(time: p * duration)
                this.total.text = this.format(time: duration)
                this.slider.value = Float(p)
            }).disposed(by: disposeBag)
        }
        
        @objc private func sliderChanged(_ sender: UISlider) {
            if Double(slider.value) == AppConstant.music.progress.value { return }
            AppConstant.music.seek(to: Double(sender.value))
        }
        
        /// format time interval to readable time for labels
        private func format(time: TimeInterval) -> String {
            let min = Int(time / 60)
            let sec = Int(time.truncatingRemainder(dividingBy: 60))
            return String(format: "%02i:%02i", min, sec)
        }
    }
    
    
}
