//
//  Main.musicItem.swift
//  SpotifySample
//
//  Created by MohammadReza Zamanieh on 8/17/21.
//

import Foundation
import UIKit
import RxSwift


extension Main {
    
    class currentMusic: UIView {
        
        private var container: UIView!
        private var progress: UIView!
        private var currentProgress: UIView!
        private var favBtn: UIButton!
        private var titleLabel: UILabel!
        private var btn: UIButton!
        private var playBtn: UIButton!
        private var sep: UIView!
        
        private var music: Model.music
        private var album: Model.album
        private let disposeBag = DisposeBag()
        
        private var width: NSLayoutConstraint!
        
        init(music: Model.music, album: Model.album) {
            self.music = music
            self.album = album
            super.init(frame: .zero)
            self.initialize()
        }
        
        required init?(coder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        // MARK: - FUNCTIONS
        private func initialize() {
            self.backgroundColor = UIColor.app.black.value
            
            container = UIView()
            container.backgroundColor = .clear
            self.addSubview(container)
            container.constrain(to: self).leadingTrailing().top()
            container.constrainSelf().height(constant: AppConstant.k.currentMusicHeight)
            
            progress = UIView()
            progress.backgroundColor = UIColor.app.gray.value
            self.container.addSubview(progress)
            progress.constrain(to: self.container).leadingTrailing().top()
            progress.constrainSelf().height(constant: 2)
            
            currentProgress = UIView()
            currentProgress.backgroundColor = UIColor.app.pure_white.value
            currentProgress.layer.cornerRadius = 2
            self.container.addSubview(currentProgress)
            currentProgress.constrain(to: self.container).leading().top(constant: -1)
            currentProgress.constrainSelf().height(constant: 4)
            width = currentProgress.constrainSelf().width(constant: 0).constraints.first
            width.isActive = true
            
            
            let main = "\(self.music.title) • \(self.music.artist.name)"
            let text = " • \(self.music.artist.name) "
            let range = (main as NSString).range(of: text)
            let attr = NSMutableAttributedString.init(string: main)
            attr.addAttributes([.foregroundColor: UIColor.app.lightGray.value], range: range)
            
            titleLabel = UILabel()
            titleLabel.attributedText = attr
            titleLabel.textColor = UIColor.app.pure_white.value
            titleLabel.numberOfLines = 0
            titleLabel.textAlignment = .center
            titleLabel.font = UIFont.systemFont(ofSize: 12)
            self.container.addSubview(titleLabel)
            titleLabel.constrain(to: self.container).topBottom(constant: 8).centerX()
            
            btn = UIButton()
            btn.setTitle(nil, for: .normal)
            btn.setImage(nil, for: .normal)
            btn.addTarget(self, action: #selector(btnTapped(_:)), for: .touchUpInside)
            self.container.addSubview(btn)
            btn.constrain(to: self.container).leadingTrailingTopBottom()
            
            favBtn = UIButton()
            favBtn.setTitle(nil, for: .normal)
            favBtn.setImage(music.isFav ? UIImage.app.favorite.value : UIImage.app.unFavorite.value, for: .normal)
            favBtn.tintColor = self.music.isFav ? .app.red.value : .app.pure_white.value
            favBtn.addTarget(self, action: #selector(favBtnTapped(_:)), for: .touchUpInside)
            favBtn.imageView?.contentMode = .scaleAspectFit
            self.container.addSubview(favBtn)
            favBtn.constrain(to: self.container).leading(constant: 16).topBottom(constant: 12)
            favBtn.constrain(to: self.titleLabel).xAxis(.trailing, to: .leading, relation: .lessThanOrEqual, constant: -12)
            
            playBtn = UIButton()
            playBtn.setTitle(nil, for: .normal)
            playBtn.addTarget(self, action: #selector(playBtnTapped(_:)), for: .touchUpOutside)
            playBtn.tintColor = .app.pure_white.value
            playBtn.addTarget(self, action: #selector(playBtnTapped(_:)), for: .touchUpInside)
            playBtn.imageView?.contentMode = .scaleAspectFit
            self.container.addSubview(playBtn)
            playBtn.constrain(to: self.container).trailing(constant: 16).topBottom(constant: 12)
            playBtn.constrain(to: self.titleLabel).xAxis(.leading, to: .trailing, relation: .greaterThanOrEqual, constant: 12)
            
            sep = UIView()
            sep.backgroundColor = .app.pure_black.value
            self.addSubview(sep)
            sep.constrain(to: self).leadingTrailing().bottom()
            sep.constrainSelf().height(constant: 1)
            
            AppConstant.music.progress.subscribe(onNext: {[weak self] progress in
                guard let this = self else { return }
                guard let p = progress else { return }
                let w = Z.math.lMapValue(value: CGFloat(p), srcLow: 0, srcHigh: 1, dstLow: 0, dstHigh: this.frame.size.width)
                this.width.constant = w
            }).disposed(by: disposeBag)
            
            AppConstant.music.isPlaying.subscribe(onNext: {[weak self] isPlaying in
                guard let this = self else { return }
                this.playBtn.setImage(isPlaying ? UIImage.app.pause.value : UIImage.app.play.value, for: .normal)
            }).disposed(by: disposeBag)
            
            AppConstant.music.current.subscribe(onNext: {[weak self] music in
                guard let this = self else { return }
                guard let m = music else { return }
                this.music = m
            }).disposed(by: disposeBag)
            
        }
        
        @objc private func favBtnTapped(_ sender: UIButton) {
            self.music.isFav.toggle()
            self.favBtn.setImage(music.isFav ? UIImage.app.favorite.value : UIImage.app.unFavorite.value, for: .normal)
            self.favBtn.tintColor = self.music.isFav ? .app.red.value : .app.pure_white.value
        }
        
        @objc private func playBtnTapped(_ sender: UIButton) {
            let isPlaying = AppConstant.music.isPlaying.value
            isPlaying ? AppConstant.music.pause() : AppConstant.music.play()
        }
        
        @objc private func btnTapped(_ sender: UIButton) {
            let vc = Player.vc.init(music: self.music, album: self.album)
            Z.ui.topController?.present(vc, animated: true, completion: nil)
        }
        
    }
    
    
}
