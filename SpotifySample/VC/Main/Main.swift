//
//  Main.swift
//  Eddi-Bike
//
//  Created by MohammadReza Zamanieh on 7/11/21.
//

import Foundation
import UIKit
import RxSwift

struct Main { }


extension Main {
    
    class vc: UIViewController, TabbarDelegate, UINavigationControllerDelegate {
        
        private var homeNav: BaseNavC!
        private var home: Home.vc!
        
        private var container: UIView!
        private var tabbar: Tabbar!
        private var music: currentMusic?
        
        private var tabbarBottom: NSLayoutConstraint!
        private var musicBottom: NSLayoutConstraint?
        private var musicHeight: NSLayoutConstraint?
        private var lastShownVC: UIViewController?
        private var lastIndex: Int?
        
        private var disposeBag = DisposeBag()
        
        override func viewDidLoad() {
            super.viewDidLoad()
            self.initialize()
        }
        
        // MARK: - FUNCTIONS
        private func initialize() {
            self.view.frame = UIScreen.main.bounds
            self.view.backgroundColor = .clear
            
            home = Home.vc()
            homeNav = BaseNavC.init(rootViewController: home)
            homeNav.delegate = self
            
            container = UIView()
            container.backgroundColor = UIColor.app.pure_black.value
            self.view.addSubview(container)
            container.constrain(to: self.view).leadingTrailingTopBottom()
            
            tabbar = Tabbar()
            tabbar.delegate = self
            self.view.addSubview(tabbar)
            tabbar.constrain(to: self.view).leadingTrailing()
            tabbarBottom = tabbar.constrain(to: self.view).bottom().constraints.first
            tabbarBottom.isActive = true
            
            tabbar.configure(with: [.init(image: UIImage.app.home.value, title: "Home", index: 0), .init(image: UIImage.app.search.value, title: "Search", index: 1), .init(image: UIImage.app.library.value, title: "Library", index: 2), .init(image: UIImage.app.premium.value, title: "Premium", index: 3)])
            
            self.tabbar.select(index: 0)
            
            
            AppConstant.music.current.subscribe(onNext: {[weak self] music in
                guard let this = self else { return }
                if let m = music, let a = AppConstant.music.album {
                    this.show(current: m, with: a)
                } else {
                    this.removeMusic()
                }
            }).disposed(by: disposeBag)

        }
        
        /// remove last music and show new music
        private func show(current: Model.music,with album: Model.album, animated: Bool = true) {
            self.removeMusic(animated: animated, completion: {[weak self] in
                guard let this = self else { return }
                let defaultHeight = AppConstant.k.currentMusicHeight
                this.music = .init(music: current, album: album)
                this.view.addSubview(this.music!)
                this.view.bringSubviewToFront(this.tabbar)
                this.music?.constrain(to: this.view).leadingTrailing()
                this.musicHeight = this.music?.constrainSelf().height(constant: this.tabbarBottom.constant == 0 ? defaultHeight : defaultHeight + UIDevice.current.bottomNotch).constraints.first
                this.musicHeight?.isActive = true
                this.musicBottom = this.music?.constrain(to: this.tabbar).yAxis(.bottom, to: .top, constant: -50).constraints.first
                this.musicBottom?.isActive = true
                this.musicBottom?.constant = 0
                UIView.animate(withDuration: animated ? 0.3 : 0, animations: {
                    this.view.layoutIfNeeded()
                })
            })
        }
        
        /// remove last music
        private func removeMusic(animated: Bool = true, completion: (() -> Void)? = nil) {
            guard musicBottom != nil, musicBottom?.constant != -50 else { completion?(); return }
            musicBottom?.constant = -50
            UIView.animate(withDuration: animated ? 0.3 : 0, animations: {
                self.view.layoutIfNeeded()
            }, completion: { _ in
                self.music?.removeFromSuperview()
                self.musicBottom = nil
                self.musicHeight = nil
                self.music = nil
                completion?()
            })
        }
        
        /// position for tabbar and current music if shown
        private func positionBottom(for nav: UINavigationController) {
            let shouldHide = nav.viewControllers.count > 1
            let finalBottomInset = shouldHide ? -(UITabBar.tabBarHeight + UIDevice.current.bottomNotch) : 0
            if finalBottomInset == self.tabbarBottom.constant { return }
            self.tabbarBottom.constant = finalBottomInset
            self.musicHeight?.constant = shouldHide ? AppConstant.k.currentMusicHeight + UIDevice.current.bottomNotch : AppConstant.k.currentMusicHeight
            UIView.animate(withDuration: 0.3, animations: {
                self.view.layoutIfNeeded()
            })
        }
        
        /// change current controller
        private func change(child vc: UIViewController) {
            if vc == lastShownVC { return }
            if let vc = lastShownVC {
                self.removeVC(vc)
            }
            self.add(child: vc)
        }
        
        /// add new controller
        private func add(child vc: UIViewController) {
            self.lastShownVC = vc
            self.addChildVC(containerView: self.container, vc)
        }
        
        /// remove controller
        private func removeVC(_ vc: UIViewController, completion: (() -> Void)? = nil) {
            guard self.container.subviews.count > 0 else { return }
            self.removeChildVC(vc, completion: completion)
        }
        
        /// change current tab
        public func go(to index: Int) {
            self.tabbar.select(index: index)
        }
        
        // MARK: - TABBAR DELEGATE
        func tabbar(_ bar: Main.Tabbar, didSelectItemAt index: Int) {
            if index == lastIndex { return }
            switch index {
            case 0:
                self.change(child: self.homeNav)
                self.lastIndex = index
            default:
                self.go(to: 0)
            }
        }
        
        // MARK: - UINAVIGATION CONTROLLER DELEGATE
        func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool) {
            self.positionBottom(for: navigationController)
        }
        
        func navigationController(_ navigationController: UINavigationController, didShow viewController: UIViewController, animated: Bool) {
            self.positionBottom(for: navigationController)
        }
        
    }
    
}
