//
//  AppDelegate.swift
//  SpotifySample
//
//  Created by MohammadReza Zamanieh on 8/17/21.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    /// main window of application
    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.makeKeyAndVisible()
        
        AppConstant.shared._initialize()
        
        return true
    }


}

